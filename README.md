# Ruslan Kladko

## How to find me

* Email: rkladko@gmail.com
* Phone: +380 68 868-50-68
* Skype: live:.cid.dc20b26c89f71fdc
* Github: [@rooslunn](https://www.github.com/rooslunn)
* Gitlab: [@rooslunn](https://gitlab.com/rooslunn)
* Telegram: https://t.me/rooslunn

## Job Titles

* Middle/Senior PHP Developer
* System Architect
* Fullstack Developer

## Core Responsibilities

* Ongoing development within skill's stack
* Refactoring, profiling
* REST API design
* Technical architecture
* Communication with business stakeholders
* Transition to DevOps
* [12 Factor App](https://12factor.net/) methodology
* Tests for technical interviews
* Code Review
* Integration with web services
* Highload testing

## Core Technologies

#### [Code samples](https://gitlab.com/rooslunn/resume/-/wikis/showroom)

* Programming Languages
    * PHP
    * JavaScript
    * Python
    * C
* Frameworks and Libraries
    * Laravel, Symfony, Yii and other popular
    * jQuery, VueJS
* Paradigms and Methodologies
    * S.O.L.I.D, DRY, KISS, YAGNI
    * TDD, BDD
    * Agile, Scrum, Kanban
* Databases and Data Tools
    * MySQL
    * PostgreSQL
    * Redis
    * Memcached
* Cloud and Infrastructure Providers
    * Amazon Web Services
    * Google Cloud Platform
    * Digital Ocean
* Other Tools and Services:
    * Docker
    * Apache Solr, Apache jMeter, Sphinx

## Work Experience

### _Jan 2011 - Present_: **PHP Developer, full time remote, office**

* Kyiv, Odessa, Lviv, Bangkok

### Projects and Responsibilities

* Project's setup and architecture
* Backend and frontend from scratch
* Crawlers and scrapers
* Testing, Profiling and Code Review
* Refactoring, Legacy code support
* Integration with Web-services
* Site Reliability Engineering
* Highload testing
* Test preparation for technical interviews
* Working with international clients

### _Jan 2007 - Sep 2010_: **Head of VISA card processing center**

* Imexbank, Odessa

### Projects and Responsibilities

* Go Live VISA Card Processing (Issuing and Acquiring).
* Setup and Pass required certifications (Issuing, Acquiring)
* EMV Card Application setup and profile
* Risk Rules, Security policies for Payment Cards, PCI DSS
* Led and Assisted Network infrastructure setup, Security Modules setup
* Led Project Team (5 peoples)

### _Nov 2001 - Nov 2007_: **Software/Hardware Engineer**

* Marine Transport Bank, Odessa

### Projects and Responsibilities

* Software developing (Accounting and Reporting)
* POS/ATM service support
* Card Management Applications support

## Education

* _1993 - 1998_: **East Ukrainian National University**
* Specialist degree in IT-management (recognized in Germany)

## Professional development, courses

* 2016, stepic.org, Neural Networks
* 2016, coursera.org, Machine Learning
* 2015, Stanford(USA), Algorithms: Design and Analysis, Part 1
* 2015, codeschool.com, Codeschool Courses (Angular, Sass)
* 2014, MongoDB University, MongoDB for Developers Certificate

## Projects

* _Apr 2020 — Aug 2020_, **UnitedPrint SE, Radebeull, Germany**
    - Ongoing development
    - Bug fixing/refactoring/implement test driven development (codeception)

* _Sep 2019 — March 2020_, **Dreamscape.com (web.com)**
    - Ongoing development
    - Response time/DB optimizations
    - No frameworks, lot of legacy procedural code, so refactoring to SOLID, OOP is prerogative
    - Article Editor tool (online help editor for all apps): Laravel, MySQL, VueJS, Vanilla JS

* _Feb 2019 — May 2019_, **Spyse.com**
    - Slow queries optimisations, minimizing response time
    - CodeIgniter, PostgreSQL

* _Jul 2018 — Dec 2018_, **Pilulka.cz**
    - Implementing REST API for mobile apps
    - Ongoing development
    - Laravel, Symfony, MySQL, Elastic Search

* _Feb 2018 — Jun 2018_
    - Freelance projects
    - Skill’s improvement time - MOOC (Coursera, Stepic), AWS, GCP

* _Oct 2017 - Jan 2018_, **Academos**, Online book store
    - Unifying interface for managing suppliers files (upload, registry etc.)
    - Ongoing support, development, refactoring
    - In-house framework development, jQuery, MySQL

* _Apr 2017 - Aug 2017_, **Business VPN**
    - REST API design and implementation
    - Laravel, GraphQL, PostgreSQL, Vue.js

* _Sep 2016 - Feb 2017_, **ouffer.com**, CRM
    - Ongoing development, Refactoring
    - In-house PHP Framework, jQuery,MySQL, Sphinx

* _May 2016 - July 2016_, **Subscribeasy.com**, CRM
    - Ongoing development, Refactoring
    - Laravel, jQuery, Vue.js, MySQL

* _Jan 2016 - Feb 2016_, **Spiritum**
    - Project setup and architecture.
    - Technical interviews, hiring

* _Sep 2015 - Nov 2015_, **VKINO, vkino.com.ua**, online movie tickets
    - Migrated backend to Laravel
    - Back-end and Front-end optimisations (caching, assets management, etc.)
    - MySQL db scheme refactoring

* _Mar 2015 - Jun 2015_, **Gravity4, tr.im**, Advertising and PR Services
    - Tr.im project Ongoing development
    - Integration with other Gravity projects

* _Jul 2014 - Feb 2015_, **3DPrinterOS**
    - Ongoing development, upgrading, refactoring and adding new features
    - Integration with Stripe and i.materialize
    - PhotoBooth (self-service registration terminal for mariners),
    - VTA-Rvd (admin system for crewing agencies)

* _Feb 2014 - Jun 2014_, **12go.asia**, e-ticketing service, transfer optimization
    - Ongoing development, refactoring and adding new features
    - Integration with Google Maps and Google Search
    - Project analytics
    - Business development negotiations

* _Jul 2013 - Jan 2014_, **SystemDoc**, Moscow City Administration
    - Ongoing development, upgrading, refactoring and adding new features

* _Feb 2013 - Jul 2013_, **GPS Control**, Cargo Monitoring
    - Web and Adnroid application for monitoring and dispatching vehicle online

* _Jul 2012 - Dec 2012_, **LIKS CARD SERVICE**, Transportation and Logistics
    - “craigslist” for cargo agents
    - UI implementation (Twitter Bootstrap)
    - REST API

* _Sep 2011 - May 2012_, **Pixonic**, Game Development**
    - Designed and developed backend for game.
    - Functional testing, code optimization for DAU 100K/day
    - Documentation (class and sequence diagrams, backend's interface API)
    - Admin Panel (users, billing, initial settings)
    - High-load testing, code optimization and server tuning
